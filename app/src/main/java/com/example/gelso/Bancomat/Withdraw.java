package com.example.gelso.Bancomat;

import android.app.NotificationManager;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.gelso.bank.R;

import static android.R.attr.y;

public class Withdraw extends AppCompatActivity {

    Button btn_ok_Withdraw;
    EditText txt_withdraw;
    Button btn_cancel_Withdraw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdraw);

        layout_setting();
    }



    public void layout_setting() {
        //BTN WITHDRAW
        btn_ok_Withdraw = (Button) findViewById(R.id.btn_ok_Withdraw);
        btn_cancel_Withdraw = (Button) findViewById(R.id.btn_cancel_Withdraw);
        txt_withdraw = (EditText) findViewById(R.id.txt_withdraw);

        btn_ok_Withdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Integer.parseInt(txt_withdraw.getText().toString()) > 10) {

                    Intent i = getIntent();
                    i.putExtra("money", txt_withdraw.getText().toString());
                    setResult(RESULT_OK,i);
                    finish();

                }else{
                    //PUSH NOTIFICATION
                    NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(getApplicationContext())
                                    .setSmallIcon(R.drawable.notification_icon)
                                    .setContentTitle("Cache Machine");
                    NotificationManager mNotificationManager =(NotificationManager) getSystemService(getApplicationContext().NOTIFICATION_SERVICE);
                    mNotificationManager.notify(0, mBuilder.build());
                }
            }
        });

        btn_cancel_Withdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

    }


}
